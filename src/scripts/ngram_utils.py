def tokenizeSent(text, tokenizer):
    """split text into sentences.
    - split by newlines because mwtokenizer does not split by newline
    - extract sentences using mwtokenizer
    """
    for line in text.split('\n'):
        if line and len(line) > 0:
            for sent in tokenizer.sentence_tokenize(line, use_abbreviation=True):
                yield sent


def get_tokens(sent, tokenizer):
    """tokenize a sentence.
    e.g: "Berlin, Germany" tokenizes to ["Berlin", ",", " ", "Germany"]
    """
    return list(tokenizer.word_tokenize(sent, use_abbreviation=True))


def get_ngrams(tokens, n):
    """concatenate n non-whitespace tokens"""
    for i_start, w_start in enumerate(tokens):
        if w_start == " ":
            continue
        gram = ""
        gram_count = 0
        for j in range(i_start, len(tokens)):
            w = tokens[j]
            gram += w
            if w != " ":
                gram_count += 1
            if gram_count == n:
                yield gram
                break